package org.mik.zoo.livingbeing;

public abstract class AbstractLivingbeing implements Livingbeing {
	
	private Integer id;
	private String scientificName;
	private String instanceName;
	private String imageURI;
	
	public AbstractLivingbeing() {}

	public AbstractLivingbeing(Integer id, String scientificName, String instanceName, String imageURI) {
		super();
		this.id = id;
		this.scientificName = scientificName;
		this.instanceName = instanceName;
		this.imageURI = imageURI;
	}

	public AbstractLivingbeing(String scientificName) {
		this(null, scientificName, null, null);
	}

	public Integer getId() {
		return this.id;
	}

	public String getScientificName() {
		return scientificName;
	}

	public void setScientificName(String scientificName) {
		this.scientificName = scientificName;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public String getImageURI() {
		return imageURI;
	}

	public void setImageURI(String imageURI) {
		this.imageURI = imageURI;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imageURI == null) ? 0 : imageURI.hashCode());
		result = prime * result + ((instanceName == null) ? 0 : instanceName.hashCode());
		result = prime * result + ((scientificName == null) ? 0 : scientificName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractLivingbeing other = (AbstractLivingbeing) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imageURI == null) {
			if (other.imageURI != null)
				return false;
		} else if (!imageURI.equals(other.imageURI))
			return false;
		if (instanceName == null) {
			if (other.instanceName != null)
				return false;
		} else if (!instanceName.equals(other.instanceName))
			return false;
		if (scientificName == null) {
			if (other.scientificName != null)
				return false;
		} else if (!scientificName.equals(other.scientificName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AbstractLivingbeing [scientificName=" + this.scientificName + ", instanceName=" + this.instanceName + "]";
	}	

}

package org.mik.zoo.livingbeing.animal;

import org.mik.zoo.livingbeing.AbstractLivingbeing;

public abstract class AbstractAnimal 
				extends AbstractLivingbeing
				implements Animal{
	
	private int numberOfLegs;
	private int numberOfTeeth;
	private int weight;
	private AnimalType animalType;
	
	public AbstractAnimal(Integer id, String scientificName,
			String instanceName, String imageURI,
			int numberOfLegs, int numberOfTeeth, int weight, 
			AnimalType animalType) {
		super(id, scientificName, instanceName, imageURI);
		this.numberOfLegs = numberOfLegs;
		this.numberOfTeeth = numberOfTeeth;
		this.weight = weight;
		this.animalType = animalType;
	}

	public int getNumberOfLegs() {
		return this.numberOfLegs;
	}

	public void setNumberOfLegs(int numberOfLegs) {
		this.numberOfLegs = numberOfLegs;
	}

	public int getNumberOfTeeth() {
		return this.numberOfTeeth;
	}

	public void setNumberOfTeeth(int numberOfTeeth) {
		this.numberOfTeeth = numberOfTeeth;
	}

	public int getWeight() {
		return this.weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public AnimalType getAnimalType() {
		return this.animalType;
	}

	public void setAnimalType(AnimalType animalType) {
		this.animalType = animalType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((animalType == null) ? 0 : animalType.hashCode());
		result = prime * result + numberOfLegs;
		result = prime * result + numberOfTeeth;
		result = prime * result + weight;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractAnimal other = (AbstractAnimal) obj;
		if (animalType != other.animalType)
			return false;
		if (numberOfLegs != other.numberOfLegs)
			return false;
		if (numberOfTeeth != other.numberOfTeeth)
			return false;
		if (weight != other.weight)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AbstractAnimal [numberOfLegs=" + this.numberOfLegs + ", numberOfTeeth=" + this.numberOfTeeth + ", weight="
				+ this.weight + ", animalType=" + this.animalType + "]";
	}
	
	
	
	
	
	
}

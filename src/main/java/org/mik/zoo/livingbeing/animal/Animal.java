package org.mik.zoo.livingbeing.animal;

import org.mik.zoo.livingbeing.Livingbeing;

public interface Animal extends Livingbeing {
	
	int getNumberOfLegs();
	int getNumberOfTeeth();
	int getWeight();
	AnimalType getAnimalType();

}
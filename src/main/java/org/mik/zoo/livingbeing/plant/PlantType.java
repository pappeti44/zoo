package org.mik.zoo.livingbeing.plant;

public enum PlantType {
	ANGIOSPERM, GYMNOSPERM;
}

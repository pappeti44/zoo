package org.mik.zoo.livingbeing.plant;

import org.mik.zoo.livingbeing.AbstractLivingbeing;

public abstract class AbstractPlant extends AbstractLivingbeing implements Plant{
	
	private int numberOfFloralElement;
	private int numberOfCotyledon;
	private PlantType plantType;
	
	public AbstractPlant(Integer id, String scientificName,
			String instanceName, String imageURI,
			int numberOfFloralElement, int numberOfCotyledon, 
			PlantType plantType) {
		super(id, scientificName, instanceName, imageURI);
		this.numberOfFloralElement = numberOfFloralElement;
		this.numberOfCotyledon = numberOfCotyledon;
		this.plantType = plantType;
	}

	public int getNumberOfFloralElement() {
		return this.numberOfFloralElement;
	}

	public void setNumberOfFloralElement(int numberOfFloralElement) {
		this.numberOfFloralElement = numberOfFloralElement;
	}

	public int getNumberOfCotyledon() {
		return this.numberOfCotyledon;
	}

	public void setNumberOfCotyledon(int numberOfCotyledon) {
		this.numberOfCotyledon = numberOfCotyledon;
	}

	public PlantType getPlantType() {
		return this.plantType;
	}

	public void setPlantType(PlantType plantType) {
		this.plantType = plantType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + numberOfCotyledon;
		result = prime * result + numberOfFloralElement;
		result = prime * result + ((plantType == null) ? 0 : plantType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractPlant other = (AbstractPlant) obj;
		if (numberOfCotyledon != other.numberOfCotyledon)
			return false;
		if (numberOfFloralElement != other.numberOfFloralElement)
			return false;
		if (plantType != other.plantType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AbstractPlant [numberOfFloralElement=" + this.numberOfFloralElement + ", numberOfCotyledon="
				+ this.numberOfCotyledon + ", plantType=" + this.plantType + "]";
	}
	
	
	
	

}
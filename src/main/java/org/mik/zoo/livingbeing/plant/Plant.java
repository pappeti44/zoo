package org.mik.zoo.livingbeing.plant;

import org.mik.zoo.livingbeing.Livingbeing;

public interface Plant extends Livingbeing {
	
	int getNumberOfFloralElement();
	int getNumberOfCotydelon();
	PlantType getPlantType();

}

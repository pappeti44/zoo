package org.mik.zoo.livingbeing;

public interface Livingbeing {
	Integer getId();
	
	String getScientificName();
	
	String getInstanceName();
	
	String getImageURI();
	
	boolean isAnimal();
	
	boolean isPlant();
	
	boolean isTree();
	
	boolean isFlower();
	
	boolean isMammal();
	
	boolean isFish();
	
	boolean isBird();

}
